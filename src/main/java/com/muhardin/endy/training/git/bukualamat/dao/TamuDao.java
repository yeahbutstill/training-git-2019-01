package com.muhardin.endy.training.git.bukualamat.dao;

import com.muhardin.endy.training.git.bukualamat.entity.Tamu;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TamuDao extends PagingAndSortingRepository<Tamu, Integer> {
}
